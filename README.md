# SuperMart API


## Summary

A simple API for supermarket using MongoDB

## Development

### Requirements

+ python>=3.6
+ [pipenv](https://pipenv.readthedocs.io)
+ MongoDB
+ Gunicorn

### Install

1. Clone and change to the directory:

    ```sh
    git clone https://gitlab.com/chumaumenze/supermart.git
    cd supermart
    ```

2. Install dependencies:

    ```sh
    pip install pipenv
    pipenv install
    pipenv install --dev  # for development purpose
    ```

3. Copy the [`instance/config.sample.py`][instance_conf_sample] file to `instance/config.py`:

    ```sh
    mv instance/config.sample.py instance/config.py
    ```

#### Database

To create the database tables ...

1. Create the database:

   ```sh
   export FLASK_ENV=development
   pipenv run python manage.py install
   ```

2. Update your MongoDB config value in your `instance/config.py`
file with your local db credentials:

   ```python
    MONGO_DATABASE_HOST = "localhost" 
    MONGO_DATABASE_PORT = 27017 
    MONGO_DATABASE_NAME = "supermart_local" 
    MONGO_DATABASE_DRIVER = "mongodb"
   ```

### Run

1. Run the server:

    ```sh
    pipenv run gunicorn -b 127.0.0.1:5000 wsgi:application -k gevent
    ```

2. Navigate to [localhost:5000/](http://localhost:5000/)


#### Default Login

`username`: admin

`password`: admin


[instance_conf_sample]: ./instance/config.sample.py
