import os

from application import create_app
from application.utils.request_response_helpers import api_success_response

application = create_app()


@application.route("/", methods=["GET"], strict_slashes=False)
def api_test():
    """Return API connectivity test response"""
    return api_success_response(response_data=None,
                                message="SuperMart API test successful")


@application.route("/map")
def api_map():
    links = {
        rule.endpoint: {
            'endpoint': rule.rule,
            'name': rule.endpoint,
            'methods': list(rule.methods),
            'arguments': list(rule.arguments)
        }
        for rule in application.url_map.iter_rules()
    }

    return api_success_response(response_data=links)


if __name__ == "__main__":
    host = application.config.setdefault("HOST_IP", "0.0.0.0")
    port = application.config.setdefault("HOST_PORT", 5000)
    prefix = application.config["API_PREFIX"]

    print(f"API running at http://{host}:{port}{prefix}")  # noqa
    print(f"Web running at http://{host}:{port}")  # noqa

    application.run(host=host, port=port)
