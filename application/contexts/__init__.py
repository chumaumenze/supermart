from flask import session


def before_every_request():
    """Do some necessary setup before handling any request."""
    pass


def after_every_request(response):
    """Do necessary operations after every request."""
    return response
