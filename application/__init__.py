import os

from flask import Flask, g
from mongoengine import connect

from application.utils.config_modes import ProductionConfig, DevelopmentConfig


def setup_core_tools(app):
    from application.errors.handlers import setup_error_handling

    setup_error_handling(app)


def _setup_blueprints(app):
    from application.controllers.api_v1 import api_v1
    from application.contexts import before_every_request
    from application.contexts import after_every_request

    api_v1_prefix = app.config["API_PREFIX"]

    app.register_blueprint(api_v1, url_prefix=api_v1_prefix)

    # Bind before request context handlers to blueprints
    app.before_request_funcs.update({
        api_v1.name: [before_every_request],
    })

    # Bind after request context handlers to blueprints
    app.after_request_funcs.update({
        api_v1.name: [after_every_request],
    })


def create_app():
    """Create a Flask application instance."""
    app = Flask(__name__, instance_relative_config=True)

    app_mode = os.environ["FLASK_ENV"]
    configuration = DevelopmentConfig if app_mode == "development" else ProductionConfig

    # Load the configuration settings
    app.config.from_object(configuration)
    app.config.from_pyfile("config.py", silent=True)

    connect(
        db=app.config["MONGO_DATABASE_NAME"],
        host=app.config["MONGO_DATABASE_HOST"],
        port=app.config["MONGO_DATABASE_PORT"]
    )

    setup_core_tools(app)
    _setup_blueprints(app)

    return app
