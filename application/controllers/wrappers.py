from functools import wraps

from flask import request, session
from flask_httpauth import HTTPBasicAuth

from application import errors
from application.models import User

# Let"s use a Basic Auth
api_auth = HTTPBasicAuth()


@api_auth.verify_password
def verify_password(username, password):
    """Verify user login details and intitates session if successful"""
    # if session.get("user") is not None:
    #     return True

    # Ensure username and password was submitted
    if not (username and password):
        # Validate using API key
        api_key = request.headers.get("SPM-Key")
        return validate_api_key(api_key)

    user = User.objects(username=username).first()
    if user is None:
        raise errors.UnsuccessfulAuthentication

    if not user.verify_password(password):
        raise errors.UnsuccessfulAuthentication

    if not user.is_active:
        raise errors.UnauthorizedUser

    session["user"] = user.to_dict()
    # session["user"] = user

    return True


def validate_api_key(api_key):
    user = User.objects(api_key=api_key).first()

    if user is None:
        raise errors.UnsuccessfulAuthentication
    if not user.is_active:
        raise errors.UnauthorizedUser

    session["user"] = user.to_dict()
    # session["user"] = user

    return True


def request_data_required(*required_data):
    """Check if request body contains [valid] request data"""

    def decorated_func(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            data = request.json
            if required_data:
                missing_data = set(required_data) - set((data or {}).keys())
                if missing_data:
                    raise errors.InvalidRequestData(
                        f"Required payload: {list(missing_data)}")

            return f(*args, **kwargs)

        return wrapper

    return decorated_func
