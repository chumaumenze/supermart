from flask import Blueprint
from application.utils.request_response_helpers import api_success_response

api_v1 = Blueprint("api_v1", __name__)

from .routes import *


@api_v1.route("/", methods=["GET"], strict_slashes=False)
def api_test():
    return api_success_response(response_data=None,
                                message="SuperMart API v1 test successful")
