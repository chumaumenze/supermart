from flask import Blueprint, session, request

from application.errors import UnsuccessfulAuthentication
from application.utils.request_response_helpers import api_success_response
from ..helpers import create_user, delete_product, get_user_by_username
from ..helpers import get_all_products, modify_product, get_payments
from ..helpers import get_product_by_uid, make_payments
from ..wrappers import request_data_required, api_auth

api_v1 = Blueprint("api_v1", __name__, template_folder="templates")


@api_v1.route("/authentication", methods=["GET"])
@api_auth.login_required
def authenticate():
    authenticated_user = session.get("user")
    if authenticated_user is None:
        raise UnsuccessfulAuthentication

    data = {"user": authenticated_user}
    return api_success_response(response_data=data, message="Login successful")


@api_v1.route("/users/<string:username>", methods=["GET"])
@api_auth.login_required
def get_user(username):
    user = get_user_by_username(username)
    return api_success_response(response_data=user.to_dict(),
                                message="User creation successful")


@api_v1.route("/users", methods=["PUT"])
@request_data_required("username", "email", "password")
def register_new_user():
    user_details = dict(
        username=request.json["username"],
        email=request.json["email"],
        password=request.json["password"],
        first_name=request.json.get("first_name"),
        last_name=request.json.get("last_name"),
        phone=request.json.get("phone"),
        address=request.json.get("address")
    )

    new_user = create_user(**user_details)
    new_user_data = new_user.to_dict()
    new_user_data["api_key"] = new_user.api_key
    return api_success_response(response_data=new_user_data,
                                message="User creation successful")


@api_v1.route("/products", methods=["GET"])
@api_auth.login_required
def get_products():
    all_products = [p.to_dict() for  p in get_all_products()]
    return api_success_response(response_data=all_products)


@api_v1.route("/products/<string:product_uid>", methods=["POST"])
@request_data_required("name", "price")
@api_auth.login_required
def update_products(product_uid):
    name = request.json["name"]
    price = request.json["price"]
    description = request.json.get("description")

    product = modify_product(product_uid, name, price, description)
    return api_success_response(message="Product updated successfully",
                                response_data=product.to_dict())


@api_v1.route("/products/<string:product_uid>", methods=["GET"])
@api_auth.login_required
def get_single_product(product_uid):
    product = get_product_by_uid(product_uid)
    return api_success_response(response_data=product.to_dict())


@api_v1.route("/products/<string:product_uid>", methods=["DELETE"])
@api_auth.login_required
def delete_products(product_uid):
    delete_product(product_uid)
    return api_success_response(message="Product deleted successfully",
                                response_data=None)


@api_v1.route("/payments", methods=["GET"])
@api_auth.login_required
def get_purchases():
    from_date = request.args.get("from_date")
    to_date = request.args.get("to_date")
    product_uid = request.args.get("product_uid")

    products = get_payments(product_uid, from_date, to_date)
    payments = [p.to_dict() for p in products]
    return api_success_response(response_data=payments)


@api_v1.route("/payments", methods=["PUT"])
@request_data_required("product_uid", "amount")
@api_auth.login_required
def make_purchase():
    product_uid = request.json["product_uid"]
    kobo_amount = request.json["amount"]

    new_payment = make_payments(kobo_amount, product_uid)
    return api_success_response(response_data=new_payment.to_dict(),
                                message="Payment successful")
