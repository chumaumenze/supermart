from datetime import datetime

from flask import session
from mongoengine.errors import NotUniqueError

from application import errors
from application.utils.helpers import kobo_to_naira
from application.models import Product, User, Payment, Status, UserCategory


def create_user(username, password, **user_details):
    user_category = UserCategory.objects(name="Customer").first()

    new_password = User.generate_password(password)
    new_user = User(username=username, password_hash=new_password,
                    user_category=user_category)
    new_user.generate_api_key()
    new_user.update(**user_details, _save=False)

    # if new_password is not None:
    #     new_user.set_password(password)
    try:
        new_user.save()

    except NotUniqueError:
        raise errors.UserAlreadyExist

    return new_user


def get_user_by_username(username):
    user = User.objects(username=username).first()
    if user is not None:
        return user

    raise errors.ResourceNotFound("User not found")


def add_product(**product_details):
    new_product = Product(**product_details)
    new_product.save()
    return new_product


def modify_product(product_uid, name, price, description):
    try:
        product = Product.objects(pk=product_uid).first()
        if product is None:
            raise ValueError

    except Exception:
        raise errors.ResourceNotFound("Product not found")

    else:
        naira_amount = kobo_to_naira(price)
        return product.update(name=name, price=naira_amount, description=description)


def delete_product(product_uid):
    try:
        product = Product.objects(pk=product_uid).first()
        if product is None:
            raise ValueError

    except Exception:
        raise errors.ResourceNotFound("Product not found")

    else:
        return product.delete()


def get_all_products():
    return Product.objects


def get_product_by_uid(product_uid):
    try:
        product = Product.objects(pk=product_uid)
    except Exception:
        raise errors.ResourceNotFound("Product not found")
    else:
     return product


def get_payments(product_uid, from_date, to_date):
    payments_data = Payment.objects

    if product_uid is not None:
        try:
            product =Product.objects(id=product_uid).first()
            if product is None:
                raise ValueError

        except Exception:
            raise errors.ResourceNotFound("Product not found")

        payments_data = payments_data(product=product)

    try:
        if from_date is not None:
            from_date = datetime.strptime(from_date, "%d-%m-%Y")
            payments_data = payments_data.filter(created_at__gte=from_date)

        if to_date is not None:
            to_date = datetime.strptime(to_date, "%d-%m-%Y").replace(
                hour=23, minute=59, second=59, microsecond=999999)
            payments_data = payments_data.filter(created_at__lte=to_date)

    except (ValueError, TypeError):
        raise errors.InvalidRequestData("Invalid date format: Use 'dd-mm-yyyy' format")

    return payments_data


def make_payments(kobo_amount, product_uid):
    product = Product.objects(id=product_uid).first()
    current_user = User.objects(username=session["user"]["username"]).first()

    #TODO: Set status to active. Fix later
    status = Status.objects(name="Active").first()
    if product is not None:
        naira_amount = kobo_to_naira(kobo_amount)

        if product.price != naira_amount:
            raise errors.InvalidRequestData(
                "Invalid amount: {name} costs {amount}".format(
                    name=product.name, amount=product.price)
            )

        return Payment(amount=naira_amount, product=product, status=status,
                       created_by=current_user).save()

    raise errors.ResourceNotFound("Product not found")
