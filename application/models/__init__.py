from datetime import datetime

from application.utils.helpers import generate_unique_reference

from mongoengine import Document, DateTimeField, StringField, BooleanField
from mongoengine import ReferenceField, DecimalField, EmailField
from werkzeug.security import check_password_hash, generate_password_hash

from application.constants import (VERY_SHORT_TEXT_LENGTH, SHORT_TEXT_LENGTH,
                                   TINY_TEXT_LENGTH, MEDIUM_TEXT_LENGTH,
                                   LONG_TEXT_LENGTH, VERY_LONG_TEXT_LENGTH)


class BaseModel(Document):
    meta = {"abstract": True} #, allow_inheritance": True}

    created_at = DateTimeField(required=True, default=datetime.now)
    modified_at = DateTimeField()
    created_by = ReferenceField("User")
    modified_by = ReferenceField("User", dbref=True)
    is_active = BooleanField(required=True, default=True)

    @classmethod
    def get(cls, is_active=True, **kwargs):
        return cls.objects(is_active=is_active, **kwargs)

    def update(self, _save=True, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

        if _save:
            self.save()

        return self

    def to_dict(self):
        raise NotImplementedError


class Status(BaseModel):
    name = StringField(max_length=VERY_SHORT_TEXT_LENGTH, unique=True)
    description = StringField(max_length=MEDIUM_TEXT_LENGTH)


class UserCategory(BaseModel):
    name = StringField(max_length=VERY_SHORT_TEXT_LENGTH, required=True,
                       unique=True)
    description = StringField(max_length=MEDIUM_TEXT_LENGTH)


class User(BaseModel):
    username = StringField(max_length=SHORT_TEXT_LENGTH, unique=True)
    first_name = StringField(max_length=SHORT_TEXT_LENGTH)
    last_name = StringField(max_length=SHORT_TEXT_LENGTH)
    phone = StringField(max_length=TINY_TEXT_LENGTH)
    email = EmailField(unique=True)
    address = StringField(max_length=MEDIUM_TEXT_LENGTH)
    password_hash = StringField(max_length=MEDIUM_TEXT_LENGTH)
    api_key = StringField(max_length=TINY_TEXT_LENGTH, unique=True, required=True)
    user_category = ReferenceField(UserCategory, required=True)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def generate_api_key(self):
        self.api_key = generate_unique_reference(size=12)

    @staticmethod
    def generate_password(password):
        return generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def to_dict(self):
        return {
            "uid": str(self.id),
            "username": self.username,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "phone": self.phone,
            "email": self.email,
            "address": self.address,
            "is_active": self.is_active,
            "user_category": self.user_category.name
        }


class Product(BaseModel):
    name = StringField(max_length=VERY_SHORT_TEXT_LENGTH, required=True)
    price = DecimalField(required=True)
    description = StringField(max_length=MEDIUM_TEXT_LENGTH)

    def to_dict(self):
        return {
            "uid": str(self.id),
            "name": self.name,
            "price": str(self.price),
            "description": self.description,
            "is_active": self.is_active
        }


class Payment(BaseModel):
    amount = DecimalField(max_length=VERY_SHORT_TEXT_LENGTH, required=False)
    description = StringField(max_length=MEDIUM_TEXT_LENGTH)
    product = ReferenceField(Product, required=True)
    status = ReferenceField(Status, required=True)

    def to_dict(self):
        return {
            "created_at": self.created_at,
            "created_by": self.created_by.to_dict(),
            "receipt_number": f"SPM_{str(self.id)}",
            "amount": str(self.amount),
            "product": self.product.to_dict(),
            "status": self.status.name.upper(),
            "description": self.description
        }
