from traceback import print_exc

from werkzeug.exceptions import HTTPException as WerkzeugHTTPException

from application.errors import APIError
from application.utils.request_response_helpers import api_failure_response


def catch_app_error(error):
    message = APIError.message
    code = APIError.code

    if isinstance(error, APIError):
        message = error.message
        code = error.code
    elif isinstance(error, WerkzeugHTTPException):
        message = error.description
        code = error.code

    # Print exception traceback
    print_exc()

    response = api_failure_response(error_msg=message, code=code)
    return response


def setup_error_handling(app):
    app.register_error_handler(APIError, catch_app_error)
    app.register_error_handler(WerkzeugHTTPException, catch_app_error)
    app.register_error_handler(Exception, catch_app_error)
