import os

from uuid import uuid4


class BaseConfig:
    SECRET_KEY = os.environ.get("SECRET_KEY") or uuid4().hex
    DEBUG = False
    TESTING = False

    TOKEN_SECONDS_VALID = 60 * 60 * 1
    MAX_IDLE_SECONDS = TOKEN_SECONDS_VALID


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = False

class ProductionConfig(BaseConfig):
    DEBUG = False
    TESTING = False
