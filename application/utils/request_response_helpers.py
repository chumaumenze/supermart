from collections import OrderedDict

from flask import jsonify, make_response


def api_success_response(response_data, code=200,
                         message="Request completed successfully."):
    return api_response(status="success", response_data=response_data,
                        message=message, code=code)


def api_failure_response(error_msg="Unable to complete request.", code=200):
    return api_response(status="failure", response_data=None,
                        message=error_msg, code=code)


def api_response(status, message, response_data=None, code=200):
    """JSON format the response for current request"""

    response_build = {
        "code": int(code),
        "status": status.upper(),
        "message": message,
        "data": response_data or None
    }

    return make_response(jsonify(response_build), code)
