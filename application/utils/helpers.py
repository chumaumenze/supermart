from decimal import localcontext, Decimal
from uuid import uuid4

from application import errors


def generate_unique_reference(numeric=False, size=64):
    return uuid4().int >> 128 - (128 - size) if numeric else uuid4().hex[:size]


def generate_api_key():
    return generate_unique_reference()[:12]


def kobo_to_naira(kobo_amount):
    try:
        with localcontext() as ctx:
            ctx.prec = 3  # Force Decimal values to 2 decimal places
            return Decimal("%.2f" % (int(kobo_amount) / 100.0))
    except Exception:
        raise errors.InvalidRequestData(
            "Currency conversion error: Ensure {} is in kobos".format(
                repr(kobo_amount))
        )
