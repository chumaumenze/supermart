STATUS = [
    ["name",            "description"],
    ["Active",        "Active Status"],
    ["Pending",   "Pending/Suspended"],
    ["Disabled",    "Disabled Status"],
    ["Deleted",   "Deleted/Cancelled"],
    ["Failed",      "Failed/Rejected"],
]

PRODUCTS = [
    ["name",       "description",      "price"],
    ["Shoe",                None,          150],
    ["Bag",                 None,           20],
    ["Laptop",              None,          250],
    ["Book",                None,        10.53],
]

USER_CATEGORY = [
    ["name",    "description"],
    ["Admin", "Administrator"],
    ["Manager", "Manager"],
    ["Customer", "Customer"],
]

USER = [
    ["first_name", "last_name",  "username",  "password",             "email"],
    ["Super",      "Admin",      "admin",     "admin",     "user@example.com"],
]

