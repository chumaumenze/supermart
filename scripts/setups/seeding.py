from collections import OrderedDict

from application.models import Status, User, UserCategory, Product
from application.utils.helpers import generate_unique_reference
from ..datasets import STATUS, PRODUCTS, USER_CATEGORY, USER

# Pair models with respective seed data
data_map = OrderedDict([
    (User.__name__, [User, USER]),
    (Product.__name__, [Product, PRODUCTS])
])


def initialize_model():
    setup_db()
    add_super_users()
    add_products()


def add_super_users():
    userdata_keys = USER.pop(0)
    # new_users_list = [dict(zip(userdata_keys, userdata)) for userdata in USER]
    for userdata in USER:
        new_userdata_dict = dict(zip(userdata_keys, userdata))

        password = new_userdata_dict.pop("password")
        new_userdata_dict["password_hash"] = User.generate_password(password)
        new_userdata_dict["api_key"] = generate_unique_reference(size=12)

        usercategory = UserCategory.objects(name="Admin").first()
        new_userdata_dict["user_category"] = usercategory

        User(**new_userdata_dict).save()

def add_products():
    product_data_keys = PRODUCTS.pop(0)
    for product_data in PRODUCTS:
        Product(**dict(zip(product_data_keys, product_data))).save()

def setup_db():
    data_map = OrderedDict([
        (Status.__name__, [Status, STATUS]),
        (UserCategory.__name__, [UserCategory, USER_CATEGORY])
    ])

    for model, data in data_map.values():
        data_keys = data.pop(0)

        for model_data in data:
            model(**dict(zip(data_keys, model_data))).save()
