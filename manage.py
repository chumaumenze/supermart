from flask_script import Manager, Shell, Server

from scripts.setups.seeding import initialize_model
from wsgi import application

manager = Manager(application)


@manager.shell
def _make_context():
    return dict(app=application)


manager.add_command("runserver", Server())
manager.add_command("shell", Shell(make_context=_make_context))


@manager.command
def install():
    initialize_model()


if __name__ == "__main__":
    manager.run()
